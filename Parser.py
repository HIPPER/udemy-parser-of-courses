import json
import csv
import scrapy
from scrapy.crawler import CrawlerProcess
from urllib.parse import urlencode


class UdemyParser(scrapy.Spider):

    name = 'udemy_parser'

    headers = {
        "accept": "application/json, text/plain, */*",
        "accept-encoding": "gzip, deflate, br",
        "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
        "cache-control": "no-cache",
        "pragma": "no-cache",
        "referer": "https://www.udemy.com/courses/search/?p=2&q=python&src=ukw",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-origin",
        "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36",
        "x-requested-with": "XMLHttpRequest",
        "x-udemy-cache-brand": "UAru_RU",
        "x-udemy-cache-campaign-code": "NEWLEARNINGS",
        "x-udemy-cache-device": "desktop",
        "x-udemy-cache-language": "ru",
        "x-udemy-cache-logged-in": "0",
        "x-udemy-cache-marketplace-country": "UA",
        "x-udemy-cache-modern-browser": "1",
        "x-udemy-cache-price-country": "UA",
        "x-udemy-cache-release": "c86d8c62a65c90b52b2c",
        "x-udemy-cache-user": "",
        "x-udemy-cache-version": "1"
    }

    url = 'https://www.udemy.com/api-2.0/search-courses/?'

    params = {
        'p': 1,
        'q': 'Learn Flutter & Dart to Build iOS & Android Apps Course',
        'price': 'price-free',
        'skip_price': 'true'
    }

    def start_requests(self):
        for page in range (1, 439): #439
            self.params['p'] = page

            yield scrapy.Request(
                url = self.url + urlencode(self.params),
                headers = self.headers,
                callback = self.parse
            )

    def parse(self, response):
        data = json.loads(response.text)['courses']


        for course in data:

            features = {
                'name': course['title'],
                'content_length': course['estimated_content_length']
            }
            
            if str(features['name']).find('Dart') != -1 or str(features['name']).find('Flutter') != -1:
                with open('courses.csv', 'a') as f:
                                writer = csv.DictWriter(f, ['name', 'content_length'])
                                writer.writerow(features)



        with open('courses.csv', mode='rt') as f, open('sorted_courses.csv', 'w') as final:
            writer = csv.writer(final)
            reader = csv.reader(f)

            sorted2 = sorted(reader, key=lambda row: (int(row[1])))
            for row in sorted2:
                writer.writerow(row)



if __name__ == "__main__":
    process = CrawlerProcess()
    process.crawl(UdemyParser)
    process.start()
